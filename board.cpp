#include "board.h"

/*
 * Make a standard 8x8 othello board and initialize it to the standard setup.
 */
Board::Board() {
    taken.set(3 + 8 * 3);
    taken.set(3 + 8 * 4);
    taken.set(4 + 8 * 3);
    taken.set(4 + 8 * 4);
    black.set(4 + 8 * 3);
    black.set(3 + 8 * 4);

    update_adj(3, 3);
    update_adj(3, 4);
    update_adj(4, 3);
    update_adj(4, 4);
}

/*
 * Destructor for the board.
 */
Board::~Board() {
}

/*
 * Returns a copy of this board.
 */
Board *Board::copy() {
    Board *newBoard = new Board();
    newBoard->black = black;
    newBoard->taken = taken;
    newBoard->adjacents = adjacents;
    return newBoard;
}

bool Board::occupied(int x, int y) {
    return taken[x + 8*y];
}

string * Board::hash()
{
    string * ret = new string("");
    for(int i = 0; i < 64; i++)
    {
        if(!taken[i])
            ret->append("0");
        else if(black[i])
            ret->append("1");
        else
            ret->append("2");
    }
    return ret;
}

// Returns true if point is adjacent to piece on side
bool Board::isAdjTo(int point, Side side)
{
    int x = point % 8;
    int y = point / 8;
    for(int dx = -1; dx <= 1; dx++)
    {
        for(int dy = -1; dy <= 1; dy++)
        {
            if(dx == 0 && dy == 0)
                continue;
            point = (x + dx) + 8 * (y + dy);
            if(taken[point])
            {
                if((side == BLACK && black[point]) || (side == WHITE && !black[point]))
                    return true;
            }
        }
    }
    return false;
}

// Update empty adjacents set.
void Board::update_adj(int x, int y) {
    int cur_pt = x + 8 * y;
    // remove from adjacents if the point is in it
    vector<int>::iterator it = find(adjacents.begin(), adjacents.end(), cur_pt);
    if(it != adjacents.end())
    {
        adjacents.erase(it);
    }
    for(int dx = -1; dx <= 1; dx++)
    {
        for(int dy = -1; dy <= 1; dy++)
        {
            if(dx == 0 && dy == 0) continue;

            int new_x = x + dx;
            int new_y = y + dy;

            // If the adjacent square is unoccupied, it's good
            if(onBoard(new_x, new_y) && !occupied(new_x, new_y))
            {
                int new_pt = new_x + 8 * new_y;
                vector<int>::iterator it2 = find(adjacents.begin(), adjacents.end(), new_pt);
                if(it2 == adjacents.end())
                {
                    adjacents.push_back(new_pt);
                }
            }
        }
    }
}

bool Board::get(Side side, int x, int y) {
    return occupied(x, y) && (black[x + 8*y] == (side == BLACK));
}

void Board::set(Side side, int x, int y) {
    taken.set(x + 8*y);
    black.set(x + 8*y, side == BLACK);
}

bool Board::onBoard(int x, int y) {
    return(0 <= x && x < 8 && 0 <= y && y < 8);
}

 
/*
 * Returns true if the game is finished; false otherwise. The game is finished 
 * if neither side has a legal move.
 */
bool Board::isDone() {
    return !(hasMoves(BLACK) || hasMoves(WHITE));
}

/*
 * Returns true if there are legal moves for the given side.
 */
bool Board::hasMoves(Side side) {
    vector<int>::iterator it;
    for(it = adjacents.begin(); it != adjacents.end(); it++)
    {
        int addr = *it;
        int x = addr % 8;
        int y = addr / 8;
        Move move(x, y);
        if (checkMove(&move, side)) return true;
    }
    return false;
}

/*
 * Returns true if a move is legal for the given side; false otherwise.
 */
bool Board::checkMove(Move *m, Side side) {
    // Passing is only legal if you have no moves.
    if (m == NULL) return !hasMoves(side);

    int X = m->getX();
    int Y = m->getY();

    // Make sure the square hasn't already been taken.
    if (occupied(X, Y)) return false;

    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            // Is there a capture in that direction?
            int x = X + dx;
            int y = Y + dy;
            if (onBoard(x, y) && get(other, x, y)) {
                do {
                    x += dx;
                    y += dy;
                } while (onBoard(x, y) && get(other, x, y));

                if (onBoard(x, y) && get(side, x, y)) return true;
            }
        }
    }
    return false;
}

/*
 * Modifies the board to reflect the specified move.
 */
void Board::doMove(Move *m, Side side) {
    // A NULL move means pass.
    if (m == NULL) return;

    // Ignore if move is invalid.
    if (!checkMove(m, side)) return;

    int X = m->getX();
    int Y = m->getY();
    Side other = (side == BLACK) ? WHITE : BLACK;
    for (int dx = -1; dx <= 1; dx++) {
        for (int dy = -1; dy <= 1; dy++) {
            if (dy == 0 && dx == 0) continue;

            int x = X;
            int y = Y;
            do {
                x += dx;
                y += dy;
            } while (onBoard(x, y) && get(other, x, y));

            if (onBoard(x, y) && get(side, x, y)) {
                x = X;
                y = Y;
                x += dx;
                y += dy;
                while (onBoard(x, y) && get(other, x, y)) {
                    set(side, x, y);
                    x += dx;
                    y += dy;
                }
            }
        }
    }
    set(side, X, Y);
    update_adj(X, Y);
}

/*
 * Current count of given side's stones.
 */
int Board::count(Side side) {
    return (side == BLACK) ? countBlack() : countWhite();
}

/*
 * Current count of black stones.
 */
int Board::countBlack() {
    return black.count();
}

/*
 * Current count of white stones.
 */
int Board::countWhite() {
    return taken.count() - black.count();
}



// Return value of a square
int Board::getValue(int x, int y)
{
    if((y == 1 || y == 6) && (x == 0 || x == 7))
        return CORNER_EDGE; // edge next to corner
    else if((y == 1 || y == 6) && (x == 1 || x == 6))
        return CORNER_DIAG; // square diagonally adjacent to corner
    else if((y == 0 || y == 7) && (x == 0 || x == 7))
        return CORNER;
    else if(y == 0 || y == 7 || x == 0 || x == 7)
        return EDGE;
    else
        return BASE_SCORE;
}

double Board::getScore(Side side)
{
    double ret = 0.0; 
    for(int i = 0; i < 64; i++)
    {
        int x = i % 8;
        int y = i / 8;
        if(!get(side, x, y))
            continue;
        ret += (double)getValue(x, y);
    }
    int mobility = 0;
    int frontiers = 0;
    for(vector<int>::iterator it = adjacents.begin(); it != adjacents.end(); it++)
    {
        int pt = *it;
        Move m(pt % 8, pt / 8);
        if(checkMove(&m, side)) // available move
            mobility++;
        if(isAdjTo(pt, side))
            frontiers++;
    }
    ret += mobility * 0.15;
    ret -= frontiers * 0.1;
    return ret;
}