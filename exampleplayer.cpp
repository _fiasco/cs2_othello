#include "exampleplayer.h"

map<string, int> ExamplePlayer::doTransTable()
{
    map<string, int> ret;
    ifstream is(cur_table);
    string temp = "";
    while(!is.eof())
    {
        getline(is, temp);
        // end if there's a trailing newline
        if(temp == "")
            break;
        istringstream ss(temp);
        string temp2;
        vector<string> tv;
        // split string by space delimiter
        while(getline(ss, temp2, ' '))
            tv.push_back(temp2.c_str());
        string k = tv.at(0);
        int v = atoi(tv.at(1).c_str());
        ret[k] = v;
    }
    return ret;
}

void ExamplePlayer::updateTransTable()
{
    ofstream os(cur_table);
    map<string, int>::iterator it;
    for(it = trans_table.begin(); it != trans_table.end(); it++)
    {
        pair<string, int> t = *it;
        os << t.first.c_str() << " " << t.second << endl;
    }
    os.close();
}

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {
    /*
     * TODO: Do any initialization you need to do here (setting up the board,
     * precalculating things, etc.) However, remember that you will only have
     * 30 seconds.
     */
    board = new Board();
    own_side = side;
    opp_side = (side == BLACK) ? WHITE : BLACK;

    cur_table = (own_side == BLACK) ? "trans_black.txt" : "trans_white.txt";
    trans_table = doTransTable();
}

/*

 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
    map<string, int>::iterator it;
    for(it = trans_table.begin(); it != trans_table.end(); it++)
    {
        pair<string, int> t = *it;
        string hash = t.first;
        int addr = t.second;
        INFO("%s %d", hash.c_str(), addr);
    }
}

Node * ExamplePlayer::decTree(int alts)
{
    Node * root = new Node(-1, board->copy(), own_side, board->getScore(own_side));
    INFO("made tree: current score = %.2f", root->getScore());
    vector<int>::iterator it;
    // keep track of current level on tree
    int level = 1;
    vector<Node *> * curLevel = new vector<Node *>();
    curLevel->push_back(root);
    // construct tree
    for(int step = 0; step < 2*alts; step++)
    {
        Side cur_side = (step % 2 == 0) ? own_side : opp_side; // Establish which side
        vector<Node *>::iterator li;
        vector<Node *> * newLevel = new vector<Node *>();
        // loop over moves to make children for current level
        for(li = curLevel->begin(); li != curLevel->end(); li++)
        {
            Node * curNode = *li; // Initially root
            Board * curBoard = curNode->getBoard();
            for(it = curBoard->adjacents.begin(); it != curBoard->adjacents.end(); it++)
            {
                int addr = *it;
                int x = addr % 8;
                int y = addr / 8;
                Move move(x, y);
                if (curBoard->checkMove(&move, cur_side))
                {
                    Board * tempBoard = curBoard->copy();
                    tempBoard->doMove(&move, cur_side);
                    double score = tempBoard->getScore(own_side);
                    Node * newNode = new Node(addr, tempBoard, cur_side, score, curNode->alpha, curNode->beta);
                    curNode->addChild(newNode);
                    newLevel->push_back(newNode); // add to new level
                }
            }
        }
        delete curLevel;
        curLevel = newLevel;
        level++;
    }
    delete curLevel;
    return root;
}

Move * ExamplePlayer::find_best_move(Node *root)
{

    map<Node*, bool> v;
    stack<Node*> s;
    s.push(root);
    bool nodeFull = true;
    while(!s.empty())
    {
        Node* top = s.top();
        s.pop();
        v[top] = 1;
        vector<Node*>::iterator i;
        if(top->getChildren()->empty())
        {
            double a = top->getScore();
            double b = top->getParent()->minimax;
            if(b == NULL)
            {
                top->getParent()->minimax = a;
            }
            else if (top->getSide() == own_side)
            {
                if(a < b)
                {
                    top->getParent()->minimax = a;
                }
            }
            else
            {
                if(a > b)
                    top->getParent()->minimax = a;
            }
        }
        else
        {
            nodeFull = true;
            for(i = top->getChildren()->begin(); i != top->getChildren()->end(); i++)
            {
                Node* next = *i;
                if(!v[next])
                {
                    s.push(next);
                    nodeFull = false;
                }
            }
            if(nodeFull)
            {
                double a = top->minimax;
                double b = top->getParent()->minimax;
                if(b == NULL)
                {
                    top->getParent()->minimax = a;
                }
                else if (top->getSide() == own_side)
                {
                    if(a < b)
                    {
                        top->getParent()->minimax = a;
                    }
                }
                else
                {
                    if(a > b)
                        top->getParent()->minimax = a;
                }
            }
        }
    }
    double p = root->minimax;
    vector<Node*>::iterator i;
    for(i = root->getChildren()->begin(); i != root->getChildren()->end();i++)
    {
        Node *n = *i;
        if(n->minimax == p)
            return n->getMove();
    }
    INFO("ERROR, %.2f", p);
    Node *n = *(root->getChildren()->begin());
    return n->getMove();
}

/*
void ExamplePlayer::alpha_beta_prune(Node* root, int depth, Node *nullNode)
{
    // If the depth is one, then sort the root's children, assuming the root has children.
    if(depth == 1)
    {
        if(!root->getChildren()->empty())
        {
            Node *n = *(root->getChildren()->begin());
            std::sort(root->getChildren()->begin(), root->getChildren()->end(), n->comparer);
            std::reverse(root->getChildren()->begin(), root->getChildren()->end());
        }
        return;
    }
    // If the depth is two, sort the root's children, and the children's children.
    else if(depth == 2)
    {
        vector<Node*>::iterator i;
        for(i = root->getChildren()->begin(); i != root->getChildren()->end();i++)
        {
            Node *n = *(i);
            std::sort(root->getChildren()->begin(), root->getChildren()->end(), n->comparer);
            std::reverse(root->getChildren()->begin(), root->getChildren()->end());
        }
    }
    // if the depth is two or more...
    if (depth >= 2)
    {
        Node * n = *(root->getChildren()->begin()); // The current node in DFS
        Node * lastParent = root; // The last parent visited in DFS
        vector<Node*>::iterator i;
        bool our_turn = true; // If the move made was ours, this must be true!
        while(n != NULL and n != nullNode) // Nullnode was added pre-call : like null-terminating the tree.
        {
            if(!n->getChildren()->empty() and n->check) // If the node has children and we want to check them...
            {
                our_turn = !our_turn;
                lastParent = n;
                n = *(n->getChildren()->begin());
                // n->setAlpha(lastParent->alpha); Possibly unnecessary?
                // n->setBeta(lastParent->beta);
            }
            else // The node has no children, or we don't want to check it.
            {
                if(n->check and our_turn) // If our turn, we want to change beta
                {
                    if(lastParent != NULL and n->getScore() > lastParent->beta)
                    {
                        lastParent->setBeta(n->getScore());
                    }
                }
                else if(n->check and !our_turn) // otherwise, we want to change alpha
                {
                    if(n->getScore() < lastParent->alpha)
                    {
                        lastParent->setAlpha(n->getScore());
                    }
                }
                // Regardless, let's go to the next node.
                i = find(lastParent->getChildren()->begin(), lastParent->getChildren()->end(), n);
                
                // if lastParent is root, then just move n to the next available child.
                if(lastParent == root)
                {
                    n = *(i + 1);
                }
                // Otherwise, try to move to next available child anyways.
                else if(lastParent->check and (i == (lastParent->getChildren()->end())) and ((i + 1) == (lastParent->getChildren()->end())))
                {
                    n = *(i + 1);
                }
                // OTHERWISE, we're done with this subtree. Move to last parent, and check there.
                else
                {
                    while(lastParent->check and ((i == (lastParent->getChildren()->end())) or ((i + 1) == (lastParent->getChildren()->end()))))
                    {
                        our_turn = !our_turn;
                        if(lastParent != root)
                        {
                            n = lastParent;
                            lastParent = n->getParent();
                            // Pass a-b up.
                            if(our_turn)
                            {
                                lastParent->setBeta(n->alpha);
                            }
                            else
                            {
                                lastParent->setAlpha(n->beta);
                            }
                        }
                        i = find(lastParent->getChildren()->begin(), lastParent->getChildren()->end(), n);
                    }
                    i++;
                    // We're going DOWN!
                    our_turn = !our_turn;
                    n = *i;
                    lastParent = n->getParent();
                    n->setAlpha(lastParent->alpha);
                    n->setBeta(lastParent->beta);
                }
            }
        }
    }
}
*/

/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
    /*
     * TODO: Implement how moves your AI should play here. You should first
     * process the opponent's move before calculating your own move
     */
    // process opponent's move on the board if there was one
    if(opponentsMove != NULL)
    {
        INFO("Processing opponent's move... (%d, %d)", opponentsMove->x, opponentsMove->y);
        board->doMove(opponentsMove, opp_side);
    }
    else
    {
        INFO("No opponent's move");
    }
    // no valid moves, so must pass
    if(!board->hasMoves(own_side))
    {
        INFO("No moves available!");
        return NULL;
    }
    else
    {
        INFO("Processing valid moves...");
    }
    // Check transposition table
    string * hash = board->hash();
    Move chosen(0, 0);
    if(trans_table.count(*hash) > 0)
    {
        INFO("Found move in transposition table...");
        int addr = trans_table[*hash];
        int x = addr % 8;
        int y = addr / 8;
        Move m(x, y);
        chosen = m;
    }
    else
    {
        INFO("Finding best move...");
        // otherwise, process opponent's move
        Node * decisions = decTree(2);
        chosen = *find_best_move(decisions);
    }
    INFO("Move chosen: (%d, %d)", chosen.x, chosen.y);
    // Make new pointer to move
    Move * ret = new Move(chosen.x, chosen.y);
    // Add to transposition table
    trans_table[board->hash()->c_str()] = 8 * chosen.y + chosen.x;
    updateTransTable();
    // Process own move
    board->doMove(ret, own_side);
    return ret;
}

Board * ExamplePlayer::getBoard()
{
    return board;
}
