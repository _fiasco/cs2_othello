PART 1:
Eugene established the basic implementation of the board, and Anup added in looking into future moves. Anup created the base of the decision tree, which was incorporated into the final Tree class, which then became the Node class.
Part 2:
Eugene improved work on the heuristics and added transposition table functionality, while Anup created the creation of the decision tree, dealt with memory leaks, and implemented the minimax algorithm
Through both parts, both teammates were in constant communication, and helped with all aspects of each others' code.

Improvements that we made involved a strong heuristics function, a transposition table, coupled with a decision tree. We attempted implementation of alpha-beta pruning, both in searching the decision tree and in the creation of the decision tree; however, this proved too problematic and thus we stuck to only a minimax algorithm on the decision tree. We also sped up drastically the search for available moves, only considering adjacent squares - we considered checking if squares could be moves for black versus moves for white, but realized that, computationally, this would be equivalent.