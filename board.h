#ifndef __BOARD_H__
#define __BOARD_H__

#include <iostream>
#include <bitset>
#include <vector>
#include <algorithm>
#include <cmath>
#include "common.h"
#include "stdlib.h"
#include "stdio.h"
#include "stdarg.h"
#include <cstring>
using namespace std;

// define heuristic constants
#define BASE_SCORE 1
#define EDGE 5
#define CORNER 15
#define CORNER_EDGE -5
#define CORNER_DIAG -10

#define DEBUG true
#define randint(min, max) ((int)(rand() % (max + 1 - min) + min))

inline void INFO(const char * msg, ...) {
    if(DEBUG)
    {
        va_list args;
        cerr << "yoghurt: ";
        va_start(args, msg);
        vfprintf(stderr, msg, args);
        va_end(args);
        cerr << endl;
    }
}

class Board {
   
private:
    bitset<64> black;
    bitset<64> taken;

    void update_adj(int x, int y);
       
    bool occupied(int x, int y);
    bool get(Side side, int x, int y);
    void set(Side side, int x, int y);
    bool onBoard(int x, int y);
      
public:
    Board();
    ~Board();
    Board *copy();
    string *hash();

    vector<int> adjacents;
    int getValue(int x, int y);
    double getScore(Side side);
    bool isAdjTo(int point, Side side);
        
    bool isDone();
    bool hasMoves(Side side);
    bool checkMove(Move *m, Side side);
    void doMove(Move *m, Side side);
    int count(Side side);
    int countBlack();
    int countWhite();
};

#endif
