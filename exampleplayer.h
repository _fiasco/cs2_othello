#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <iostream>
#include <fstream>
#include <sstream>
#include "common.h"
#include "board.h"
#include "stdlib.h"
#include "stdio.h"
#include <vector>
#include <stack>
#include <map>
using namespace std;


class Node {
public:
    Node(int addr, Board * b, Side si, double s = 0, double alpha = -1000, double beta = 1000)
    {
        score = s;
        move = (addr == -1) ? NULL : new Move(addr % 8, addr / 8);
        board = b;
        side = si;
        check = alpha <= beta;
        alpha = alpha;
        beta = beta;
        minimax = NULL;
        children = new vector<Node *>();

        // default to root node because we're not in a tree yet
        parent = NULL;
    }
    ~Node()
    {
        vector<Node*>::iterator child;
        for(child = children->begin(); child != children->end(); child++)
        {
            delete *child;
        }
        delete children;
    }
    void removeLastChild()
    {
        children->pop_back();
    }
    vector<Node *> * getChildren()
    {
        return children;
    }

    void setParent(Node * n)
    {
        parent = n;
    }
    Node * getParent()
    {
        return parent;
    }
    void addChild(Node * n)
    {

        children->push_back(n);
        n->setParent(this);
    }

    Board * getBoard()
    {
        return board;
    }

    double getScore()
    {
        return score;
    }
    void setAlpha(double a){alpha = a; check = alpha <= beta;}
    void setBeta(double b){beta = b; check = alpha <= beta;}
    double alpha;
    double beta;
    bool check;
    struct PointerCompare{
        bool operator() (Node* l, Node* r){
            return *l < *r;
        }
    } comparer;
    Side getSide(){return side;}
    Move* getMove()
    {
        return move;
    }

    double minimax;
private:
    double score;
    Move * move;
    Board * board;
    Side side;

    bool operator<(Node& node)
    {
        return (score < (node.getScore()));
    }
    Node * parent;
    vector<Node *> * children;
};


class ExamplePlayer {

public:
    ExamplePlayer(Side side);
    ~ExamplePlayer();

    Move* find_best_move(Node* root);
    void alpha_beta_prune(Node* node, int depth, Node * nullNode);
    map<string, int> doTransTable();
    void updateTransTable();

    Node * decTree(int alts);
    Move *doMove(Move *opponentsMove, int msLeft);

    Board * getBoard();

private:
    Board * board;
    Side own_side;
    Side opp_side;

    const char * cur_table;
    map<string, int> trans_table;
};

#endif
